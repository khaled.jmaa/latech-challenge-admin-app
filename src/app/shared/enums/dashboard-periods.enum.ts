export enum DashboardPeriods {
    WEEK = 'week',
    MONTH = 'month',
    ALL = 'all'
}