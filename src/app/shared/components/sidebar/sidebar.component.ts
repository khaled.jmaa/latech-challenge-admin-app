import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    console.log('this.route = ', this.actualRoute)
  }

  get actualRoute() {
    return this.router.url;
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
