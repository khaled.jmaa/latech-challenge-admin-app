import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(
    private httpClient: HttpClient
  ) { }

  createEvent(event: any) {
    const token = localStorage.getItem('token');
    const headers = {
      authorization: `bearer ${token}`
    };
    return this.httpClient.post(`${environment.apiUrl}/events`, event, { headers });
  }
}
