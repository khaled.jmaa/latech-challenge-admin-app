import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll() {
    const token = localStorage.getItem('token');
    const headers = {
      authorization: `bearer ${token}`
    };
    return this.httpClient.get(`${environment.apiUrl}/users`, { headers });
  }

  // deleteOne(user: any) {
  //   const token = localStorage.getItem('token');
  //   const headers = {
  //     authorization: `bearer ${token}`
  //   };
  //   return this.httpClient.delete(`${environment.apiUrl}/users/${user.id}`, { headers });
  // }
}
