import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DashboardPeriods } from '../shared/enums/dashboard-periods.enum';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getAdminStats(period: DashboardPeriods) {
    const token = localStorage.getItem('token');
    const headers = {
      authorization: `bearer ${token}`
    };

    let dateFrom;
    let dateTo;

    switch (period) {
      case DashboardPeriods.WEEK:
        dateFrom = moment().startOf('week').hours(0).minutes(0).seconds(0).format('YYYY-MM-DD HH:mm');
        dateTo = moment().endOf('week').hours(23).minutes(59).seconds(59).format('YYYY-MM-DD HH:mm');
        break;

      case DashboardPeriods.MONTH:
        dateFrom = moment().startOf('month').hours(0).minutes(0).seconds(0).format('YYYY-MM-DD HH:mm');
        dateTo = moment().endOf('month').hours(23).minutes(59).seconds(59).format('YYYY-MM-DD HH:mm');
        break;

      case DashboardPeriods.ALL:
        dateFrom = moment().year(1950).month(0).date(1).format('YYYY-MM-DD HH:mm');
        dateTo = moment().format('YYYY-MM-DD HH:mm');
        break;
    }
    const urlQuery = `?dateFrom=${dateFrom}&dateTo=${dateTo}`;

    return this.httpClient.get(`${environment.apiUrl}/stats/summary/admin${urlQuery}`, { headers });
  }
}
