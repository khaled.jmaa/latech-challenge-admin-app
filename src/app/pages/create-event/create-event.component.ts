import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { EventsService } from 'src/app/services/events.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  isSubmitted = false;
  isLoading = false;
  createEventForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private eventsService: EventsService
  ) {
    this.createEventForm = this.formBuilder.group({
      title: [''],
      description: [''],
      day: [''],
      hour: ['', [Validators.min(0), Validators.max(23)]],
      min: ['', [Validators.min(0), Validators.max(59)]],
      duration: [null],
      location: ['']
    });
  }

  ngOnInit(): void {
  }

  createEvent() {
    this.isSubmitted = true;

    if (this.createEventForm.invalid) {
      return;
    }

    // const event = this.createEventForm.getRawValue();
    const date = moment()
      .date(this.createEventForm.controls['day'].value)
      .hours(this.createEventForm.controls['hour'].value)
      .minutes(this.createEventForm.controls['min'].value);

    const event = {
      date,
      title: this.createEventForm.getRawValue().tilte,
      description: this.createEventForm.getRawValue().description,
      duration: this.createEventForm.getRawValue().duration,
      location: this.createEventForm.getRawValue().location,
    };

    this.eventsService.createEvent(event)
      .subscribe(
        (data: any) => {
          Swal.fire(
            'Event created',
            'Event successfully created',
            'success'
          )
        },
        error => {
          Swal.fire(
            'Event not created',
            '',
            'error'
          )
          this.isLoading = false;
        });
  }

}
