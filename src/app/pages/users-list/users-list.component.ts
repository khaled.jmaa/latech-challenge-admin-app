import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../app/services/users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  users: any;

  constructor(
    private usersService: UsersService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.usersService
      .getAll()
      .subscribe(
        (data: any) => {
          this.users = data.data;
        },
        error => { }
      );
  }

  deleteUser(user: any) { }

  importUsers() { }

}
