import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isSubmitted = false;
  isInvalidCredentials = false;
  isLoading = false;
  loginForm: FormGroup;

  isPasswordDisplayed = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  togglePwdDisplay() {
    this.isPasswordDisplayed = !this.isPasswordDisplayed;
  }

  get f() { return this.loginForm.controls; }
  get email() { return this.loginForm.controls['email']; }
  get password() { return this.loginForm.controls['password']; }

  onSubmit() {
    this.isSubmitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.isInvalidCredentials = false;
    this.isLoading = true;
    this.authService.login(this.email.value, this.password.value)
      .subscribe(
        (data:any) => {
          localStorage.setItem('user', JSON.stringify(data.data.user));
          localStorage.setItem('token', data.data.token);
          this.router.navigate(['/']);
        },
        error => {
          this.isInvalidCredentials = true;
          this.isLoading = false;
        });
  }
}
