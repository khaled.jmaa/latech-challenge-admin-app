import { Component, OnInit } from '@angular/core';
import { StatsService } from 'src/app/services/stats.service';
import { DashboardPeriods } from 'src/app/shared/enums/dashboard-periods.enum';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  summaryData: any;
  period: DashboardPeriods = DashboardPeriods.WEEK;

  constructor(
    private statsService: StatsService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.onPeriodChange();
  }

  onPeriodChange() {
    this.statsService
      .getAdminStats(this.period)
      .subscribe(
        (data: any) => {
          this.summaryData = data.data;
        },
        error => {

        }
      );
  }

  get periodAsSentence() {
    let text;
    switch (this.period) {
      case DashboardPeriods.WEEK: text = 'This week'; break;
      case DashboardPeriods.MONTH: text = 'This month '; break;
      case DashboardPeriods.ALL: text = 'In general'; break;
    }
    return text;
  }

  getFullname(user: any) {
    let fullname = '';
    if (!user.firstName && !user.lastName) {
      fullname = `@${user.username}`;
    } else {
      fullname = (`${user.firstName} ${user.lastName}`).trim();
    }
    return fullname;
  }

  getUserAcceptCount(user: any) {
    const count = this.summaryData
      .acceptanceAnswers
      .filter((answer: any) => answer.invited.id === user.id)
      .length;
    return count;
  }

  getUserDeclineCount(user: any) {
    const count = this.summaryData
      .unacceptanceAnswers
      .filter((answer: any) => answer.invited.id === user.id)
      .length;
    return count;
  }
}
