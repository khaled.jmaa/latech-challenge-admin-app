import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { MainComponent } from './pages/main/main.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EventsListComponent } from './pages/events-list/events-list.component';
import { CreateEventComponent } from './pages/create-event/create-event.component';
import { UsersListComponent } from './pages/users-list/users-list.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ComingSoonComponent } from './pages/coming-soon/coming-soon.component';
import { SettingsComponent } from './pages/settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    MainComponent,
    DashboardComponent,
    EventsListComponent,
    CreateEventComponent,
    UsersListComponent,
    PageNotFoundComponent,
    ComingSoonComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
