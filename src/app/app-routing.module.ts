import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEventComponent } from './pages/create-event/create-event.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EventsListComponent } from './pages/events-list/events-list.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { UsersListComponent } from './pages/users-list/users-list.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'events-list',
        component: EventsListComponent
      },
      {
        path: 'create-event',
        component: CreateEventComponent
      },
      {
        path: 'users-list',
        component: UsersListComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
    ]
  },

  {
    path: '**',
    component: PageNotFoundComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
